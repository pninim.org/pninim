% שאלה קמא

*שאלת* נצ"ב. מהו שיוכל הבעל לעשותן נכסי מלוג. 

*תשובה* 

[^1]*לכאורה* נראה דמצי למיפטר נפשיה. מאחריותן. וכן כתב הריב"ש בספר ת"ן. שיפה כח הבעל שלא לקבל עליו אחריות נצ"ב. וישארו ברשות האשה כדין נ"מ. והובא בא"ע סימן ס"ו. *ובאגרת* הרמב"ם (ד"כ) מצאתי שכתב לדיין אלכסנדריא. שדן בחלי של זהב שכתוב על החתן בתורת נצ"ב. ולא רצה החתן ליקח אותו תכשיט מיד חמיו. לפי שאמר קחו כל הנדוניא. שאיני רוצה שאהיה חייב באחריותה. שהדין עם החתן. שכופין אותה שתטול נדוניתה ותחזור הנדוניא נ"מ. ועליה השיב הרמב"ם ז"ל. שתמה ממנו על שפסק כך. שכל אשה שירצה בעלה ליתן לה נדוניתה ולא ישאר עליו אחריותה הדין עמו. ולא הביא שום ראיה לסתור פסקו של הדיין. רק היה פשוט בעיניו שטעה הדיין בדין זה. והתנצל בעדו על שלא ידע הדין יע"ש. 

[^1] שאין הבעל יכול להפקיע עצמו משעבו' נצ"ב לעשותן נ"מ: אחר שכב' נשתעבד' ומזכה רמב"ם בתשובתו


*והנה* נמצא אתי ספר האגרת הלז. וראיתי כתוב עליו מגדול הדור בגליון בזה"ל. לא מצאתי ראיה מכרעת לזה. ומה בין זה לפורע חובו קודם זמנו. ואי מאותה שאמרו לא יאמר אדם לאשתו הרי כתובתך מונחת על השלחן. ההיא בכתובה דאורייתא. וכדי שלא תהא קלה בעיניו להוציאה. עכ"ל. *ואני* בעניי נפלאתי על זאת. שדברי הרמב"ם ז"ל. ודאי נראין פשוטים ומוכרחים. דהא ודאי נצ"ב לפקדון דמו (אע"ג דלהוצאה ניתנו כמלוה. מ"מ לטובת האשה נתנום לבעל. שלא יכלה הקרן. ואינו בדין שיעשה נ"מ. שמשתמש בהן עד שכלה הקרן) דקיי"ל שאינו יכול להחזירו תוך זמנו. עבח"מ סי' רצ"ג. וכ"ש נצ"ב. דנשתעבד בתנאי כתובה ומעשה ב"ד. להתחייב באחריותן. שאינו יכול לעשותן נ"מ. בע"כ דאשה. ומשקבל עליו. ודאי אין שומעין לו. ולאו כל כמיניה דמפקע לשעבודיה. לגרוע כחה. שאם פיחתו. פיחתו לה. וזה פשוט וברור מאד בעיני. שאינו יכול להפטר מאחריות שכבר קיבל עליו. ולא כלתה שמירתו כל הזמן שאשתו יושבת תחתיו. ולכן אי אפשר לו לעשותן נ"מ אם לא ברצונה. *ומהריב"ש* אין ראיה. דמיירי כשעדיין לא קיבל הנצ"ב עליו. דודאי קודם שנתחייב בהן. יפה כחו שלא לקבלן. כי מי יכוף אותו שיקבל נדוניא. להתחייב באחריות. ובזה האופן על כרחה ישארו בידה כדין נ"מ. משא"כ בשכבר נשתעבד. כנדון שדן דיין אלכסנדריא. שכבר היה כתוב חוב עליו. וצדק הרב הגדול רמב"ם ז"ל שתמה עליו והחליט ההפך. הנלע"ד כתבתי יעב"ץ ס"ט.

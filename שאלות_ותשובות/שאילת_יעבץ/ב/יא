% שאלה יא

זה שלח אלי התו' החריף והבקי המדקדק המפורסם הרב כמו' אליהו ווילנר ע"ה ז"ל תוך אגרתו. 

להגאון מוהר"ר יעקב נר"ו אב"ד דק"ק עמדין: 

*עיני* אדמ"ו תחזינה מ"ש כו'. לעשות רצון צדיק חפצתי ביום מחר אי"ה אבוא לבית אמ"ו נר"ו כו'. בין כך אחת שאלתי. יעיין מר במ"ש הרא"ש על הא דבעי רב החליד במיעוט סימנים כו'. ופירש ר"ת החליד היינו במיעוט קמא כו'. וכן שהה במיעוט סימנים מפרש ר"ת במיעוט קמא דוושט ומיבעי' ליה דאע"ג דמיטרפא בנקיבת הוושט דילמא לא פסלה שהיה אלא במידי דמינבלה ביה כו' יע"ש. ודבריו א"א להולמן שהרי בנקיבת משהו דוושט נמי הוי נבילה ומשנה שלימה היא דף ל"ב אר"י כל שנפסלה בשחיטת נבילה והוד' לו ר"ע יע"ש. על תשובתו הרמתה אצפה שיגיד לי בע"פ ביום מחר אי"ה. 

*תשובה*

[^ פירוק תמיהא עצומה בתו' ורא"ש]*לא* ידענא מאי קשיא ליה. ודברי הרא"ש אינם אלא דברי תו' ממש. איברא לק"מ. ואין לי אלא להעתיק לו מה שכתבתי בעזה"י בחי' ע"ס הרא"ש על הלשון הנ"ל. *ז"ל* וכן שהה במעוט סימנים מפרש ר"ת במעוט קמא דושט. ומב"ל אע"ג דמטרפא בנקיבת ושט דילמא לא פסלה שהיה אלא במידי דמנבלא ביה ע"כ. וזה לשוני. *ואע"ג* דאוקימנא למתני' דאלו טרפות קודם חזרה. ההיא משום פסוקת גרגרת אתינן לה. אבל נקובת ושט עולם אימא לך אינה אלא טרפה. אף לאחר חזרה. דהא מתני' דפסול בשחיטה. לא איירי בנקובה. אלא בשחיטה. שאינה פחותה מרוב סימן. כה"ג דווקא הויא נבלה. משא"כ נקובה. לד"ה אינה אלא טרפה. ואליבא דאמת נמי. כ"ש שאינה אלא בעיא בדרך את"ל. ודוק בתו' (דלב"א) ד"ה ורמינהי. לא נקטי אלא פסוקה. ודלגי על נקובה דמיתניא ברישא. ורש"י דנסיב לנקובה. לא בדווקא. אלא אורחיה למנקט תחלת הלשון דרך קצרה. וסומך על המבין. זה ברור בס"ד. וע"ע במו"ק (ס"ך) בס"ד. גם בשי"ע (ססמ"א. דעג"א). *ובמ"ש* ניחא נמי מ"ש אמ"ה ז"ל בספר תשובותיו (סע"ז סד"ע) וז"ל וא"ת מהא דפריך בהשוחט כו'. ומאי קושיא כו' שכבר כתבו התו' כו'. *ולא* היה צריך לכך. די"ל נקובת ושט באמת אינה אלא טרפה לד"ה. וכה"ד התו' (אע"פ שאין כך דעת הר"מ) ולדידהו אפשר דמהניא שחיטה כמש"ל. מיהא מספיקא לא נפקא. ולא הויא אלא ספק נבלה. ודי בזה הקצור למבין מדעתו. ישמע חכם ויוסף לקח. נאם יעבץ ס"ט. 

אחר קבלת תשובתי: שמר עת בואו אלי לביתי. וקלס את דברי שהארתי עיניו: והגיד לי כי נשלחה לידו שאלה זו מכמו' עקיבא איגר ע"ה. שהיה אח"כ אב"ד בפ"ב.

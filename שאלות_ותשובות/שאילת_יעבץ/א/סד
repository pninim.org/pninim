% שאלה סד

[^1]*שם* שאלה ס"ג מי שבירך שאומרים החזנים בשבת אם יש לחוש משום שאין להתפלל בו. תשו' אין להתפלל על דבר שהוא צורך היום. אבל דבר שהוא להבא אין בכך כלום. ה"נ אין לאכול סמים של רפואה דווקא כשיש מיחוש. אבל בל"ז מותר עכ"ל.

[^1] תמיהא רבתא על בהל"קט במ"ש על ענין מ"ש שעושין החזנים בשבת שמהפך הסברות לעקור הלכה קבועה


*ותמיהני* ע"ז דאזיל בתר איפכא דאיברא הכא איפכא מסתברא דדבר שהוא צורך היום יש להתיר יותר. זכר לדבר מפירין נדרים ונשאלין לנדרים שהן לצורך השבת. אבל מה שאינו לצורך השבת יאסר. וכן נפסק בש"ע סי' רפ"ח בנ"ז:

*וראיה* גמורה שמה שאינו לצורך השבת אסור בתפלה. מן הירו' שהביא בטור סי' קפ"ח שהיה בדין שלא לומר בשבת. בבה"מ נוסח אבינו מלכנו רענו זונינו פרנסינו. אם לא שמטבע ברכות כך הוא. הרי שאע"פ שהוא נוסח תפלה להבא. דבודאי אינו מבקש על פרנסת היום בשבת שעומד בה. שאם לא טרח בע"ש מהיכן יאכל בשבת. וגם כבר אכל. ועל כרחין אינו אלא לשון בקשה להבא. ואעפ"כ היה מקום לאוסרו. אם לא מטעם שמטבע ברכה כך הוא:

*ומה* שדימה לסמים של רפואה לא דמי אלא כאוכלא לדנא. דסמים נאסרו משום גזרת שחיקת סממנין. משו"ה בחולה דווקא יש מקום לאוסרן. משא"כ בבריא. שבודאי לא יבא לידי כך. אבל תפלה ובקשה בשבת לא נאסרה משום גזרה דשחיקה. ומה ענין זה לזה:

[^ ופשיטא טובא דלא ש"ד מתרי טעמי]*ונ"ל* טעם מניעת הבקשה בשבת משום שלא יהא דיבורך של שבת כשל חול ויכולה היא שתרחם. לכן אין לבקש על החולה אם לא תקף עליו חליו. דצורך שעה הוא ופיקוח נפש. וכל שאין בו סכנה יש לו לבטוח בזכות שבת ולא יבקש צרכיו כדרך שעושה בחול. והיינו נמי דאמרינן דהיום הוא ראוי לתפלת י"ח. אלא משום טורח הציבור אין מתפללין בו י"ח רק ז' ברכות. דהיינו משום שנצטוינו לענג השבת. ושלא נאריך בו בבקשתנו כמו בחול. האי הוא טירחא דצבורא. ולא שנא דרך כללי. או פרטי כמו שהוא הנוסח דמי שבירך. הכל אסור. וכ"ש לבריא שאין בו צורך כלל לבו ביום. דפשיטא דאסור וק"ו מתפלת י"ח. וכמ"ש גם מהירושלמי הנ"ל. לכן אותו מנהג שנהגו לברך. טעות הוא בידי החזנים. והנושאים להם פנים. ואין להם על מה שיסמוכו. רק לטובתן ולהנאתן עושים והתברכו בלבבם למען יתברכו. ולא ילפינן ממקלקלתא. גם יש בו משום פיסוק מעות כמ"ש במקומו בס"ד וע"ש בחיבורי. אלא שאין כחנו יפה למחות מאחר שכבר נהגו משנים קדמוניות. ואשרי שיאחז במעוז הדת ונפץ עלולי המנהגים הגרועים. אל הסלעים:

[^2]*ומה* שתיקנו הראשונים יקום פורקן לכתחלה דווקא בשבת. נראה משום דבימות החול לא היו העם מתקבצים לבה"כ כמו ביש"ק שנאספין כולן. וראו לתקן ברכה כללית לכבוד תורה וללומדיה. שתהא נאמרת מפי כל הציבור כאחד. להחזיק במעוז הדת כפי צורך הדורות באורך הגלות המר. ובצרכי רבים לית לן בה. דמשו"ה דכוותה קבעי רבנן. זכרנו ומי כמוך בר"ה ויה"כ. ואף בג' ראשונות. אבל מי שברך לכל יחיד שירצה. ודאי אינו נכון כלל. ויש בו ג"כ משום טירחא דציבורא טובא. דאפי' תפלת י"ח קצרו משו"ה. ועכשיו נוהגין להאריך במ"ש ולפעמים מתעכבין עד חצות. עלבון המנהיגים הוא. אי איישר חילי אבטליניה. יעב"ץ ס"ט:

[^2] והתנצלות בעד הקדמונים שקבעו בקשת יקום פורקן ודכוותה בשבת

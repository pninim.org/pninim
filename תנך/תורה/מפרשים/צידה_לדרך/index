*ספר צידה לדרך*

מוהר"ר יששכר בער איילינבורג בעל ספר באר שבע זצ״ל.


סריקות:

<https://www.hebrewbooks.org/57972>

% זכויות יוצרים

כל הזכויות שמורות ל"פנינים". הספרים מתפרסמים תחת הרישיון החופשי GNU Free Documentation License (גרסה 1.3 עם Invariant Sections, Front-Cover Text, Back-Cover Text):

Copyright (C) 2024  Pninim <https://pninim.org>
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
published by the Free Software Foundation;
with Invariant Sections being "הקדשות", "פרויקט פנינים",
one Front-Cover Text:
"באדיבות פנינים - הספרייה החופשית pninim.org",
and one Back-Cover Text:
"באדיבות פנינים - הספרייה החופשית pninim.org
 גם אתם יכולים לסייע להרחבתה - דרך קופת הצדקה שלכם - ע"י תמיכה באברכים נזקקים שהם השוקדים על בנייתה".
A copy of the license is included in the section entitled "GNU Free Documentation License".


% פרויקט פנינים

המטרה של המיזם היא לאפשר לתלמידי חכמים נזקקים להתפרנס בכבוד תוך כדי יצירת ערך מוסף לכלל הציבור. תומכי הפרויקט זוכים לעשות צדקה בגדר של "מטיל בכיס" (שבת סג.) שהיא מעולה ממתנה (נהמא דכיסופא = "לחם של בושה") ואפילו מהלוואה.

במסגרתם הלימודית אברכים מכל רחבי הארץ עובדים על יצירת מאגר תורני ממוחשב שניתן להשתמש בו ללא תמורה (ואפילו למטרות מסחריות, בכפוף לתנאי הרישיון). ספרים עתיקים נסרקים ועוברים תהליך המרה מתמונה למסמך תמליל ממוחשב. לאחר מכן, כל טקסט עובר הגהה קפדנית - ומתפרסם לכלל הציבור באמצעות גישה מקוונת חופשית.

לפרטים נוספים נא בקרו באתרנו: <https://pninim.org>

סטיות מנוסח המקור צוינו כדלהלן:
- קטעים שלדעת העורך ראוי להחסירם מוסגרו בטילדות ~ ~
- קטעים שלדעת העורך ראוי להוסיפם מוסגרו בסוגריים מסולסלים { }

במקרה ומצאתם טעות או אי דיוק כלשהו - נבקש בכל לשון של בקשה להודיע לנו על כך בכתובת: <contact@pninim.org>


% הקדשות

- להצלחתו ולהצלחת בני משפחתו של התורם החפץ בעילום שמו ברוחניות ובגשמיות בתוך שאר עם ישראל
- לע"נ אברהם בן דוד
- לע"נ חנה בת אהרן
- להצלחת א.פ. בכל ענייניו
- נחמה בת שרה, רפואה שלמה
- לע"נ יוריי בן איליה
- לע"נ ברוניה בת לזר
- דוד בן אברהם, תשובה שלמה
- לע"נ יהודה בן שמואל
- לע"נ אסתר בת לב
- לע"נ חיה אסתר בת יעל
- טוביה בן מרים, רפואה שלמה בתוך שאר ח"י
- לע"נ אלעזר בן מנחם מנדל
- לע"נ ילנה בת יוריי

% תוכן עניינים

+<%%%>

+<שער>

+<הקדמת_בן_המחבר>

+<הקדמת_הרב_המחבר>

+<בראשית/א>

+<בראשית/ב>

+<בראשית/ג>

+<בראשית/ד>

+<בראשית/ה>

+<בראשית/ו>

+<בראשית/ז>

+<בראשית/ח>

+<בראשית/ט>

+<בראשית/י>

+<בראשית/יא>

+<בראשית/יב>

+<בראשית/יג>

+<בראשית/טו>

+<בראשית/טז>

+<בראשית/יז>

+<בראשית/יח>

+<בראשית/יט>

+<בראשית/כ>

+<בראשית/כא>

+<בראשית/כב>

+<בראשית/כג>

+<בראשית/כד>

+<בראשית/כה>

+<בראשית/כו>

+<בראשית/כז>

+<בראשית/כח>

+<בראשית/כט>

+<בראשית/ל>

+<בראשית/לא>

+<בראשית/לב>

+<בראשית/לג>

+<בראשית/לד>

+<בראשית/לה>

+<בראשית/לו>

+<בראשית/לז>

+<בראשית/לח>

+<בראשית/לט>

+<בראשית/מ>

+<בראשית/מא>

+<בראשית/מב>

+<בראשית/מג>

+<בראשית/מד>

+<בראשית/מה>

+<בראשית/מו>

+<בראשית/מז>

+<בראשית/מח>

+<בראשית/מט>

+<בראשית/נ>

+<שמות/א>

+<שמות/ב>

+<שמות/ג>

+<שמות/ד>

+<שמות/ה>

+<שמות/ו>

+<שמות/ז>

+<שמות/ח>

+<שמות/ט>

+<שמות/י>

+<שמות/יא>

+<שמות/יב>

+<שמות/יג>

+<שמות/יד>

+<שמות/טו>

+<שמות/טז>

+<שמות/יז>

+<שמות/יח>

+<שמות/יט>

+<שמות/כ>

+<שמות/כא>

+<שמות/כב>

+<שמות/כג>

+<שמות/כד>

+<שמות/כה>

+<שמות/כו>

+<שמות/כז>

+<שמות/כח>

+<שמות/כט>

+<שמות/ל>

+<שמות/לא>

+<שמות/לב>

+<שמות/לג>

+<שמות/לד>

+<שמות/לה>

+<שמות/לח>

+<שמות/מ>

+<ויקרא/א>

+<ויקרא/ב>

+<ויקרא/ג>

+<ויקרא/ד>

+<ויקרא/ה>

+<ויקרא/ו>

+<ויקרא/ז>

+<ויקרא/ח>

+<ויקרא/ט>

+<ויקרא/י>

+<ויקרא/יא>

+<ויקרא/יב>

+<ויקרא/יג>
